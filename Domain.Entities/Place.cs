﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Spatial;

namespace Domain.Entities
{
    public class Place
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public DbGeography Location { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Keyword> Keywords { get; set; }
        public virtual ICollection<Story> Stories { get; set; }

        public User Creator { get; set; }
        public User Editor { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? EditedDate { get; set; }
    }
}