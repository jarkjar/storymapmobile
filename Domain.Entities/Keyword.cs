﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Domain.Entities
{
    public class Keyword
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Story> Stories { get; set; }
        public virtual ICollection<User> UsersWhoAreInterested { get; set; }
        public virtual ICollection<Place> Places { get; set; }
        public virtual ICollection<Multimedia> Multimedia { get; set; }
        public bool Published { get; set; }
        public User Creator { get; set; }
        public User Editor { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? EditedDate { get; set; }
    }
}
