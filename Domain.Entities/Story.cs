﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Domain.Entities
{
    public class Story
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Body { get; set; }
        [Required]
        public virtual Place Place { get; set; }
        public virtual ICollection<Keyword> Keywords { get; set; }        
        //public virtual ICollection<Multimedia> Multimedia { get; set; }
        //public virtual ICollection<User> InvolvedUsers { get; set; }
        
        public bool Published { get; set; }
        public User Creator { get; set; }
        public User Editor { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? EditedDate { get; set; }
    }
}