﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entities
{
    public class Multimedia
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Extension { get; set; }
        /// <summary>
        /// Size in MB
        /// </summary>
        [Required]
        public int Size { get; set; }
        public virtual ICollection<Keyword> Keywords { get; set; }
        [Required]
        public Story Story { get; set; }

        public bool Published { get; set; }
        public User Creator { get; set; }
        public User Editor { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? EditedDate { get; set; }
    }
}