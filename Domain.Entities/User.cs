﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Domain.Entities
{
    public class User
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int FacebookId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Description { get; set; }
        //public string Password { get; set; }
        public virtual ICollection<Keyword> InterestedKeywords { get; set; }
        //public virtual ICollection<User> FollowedUserIds { get; set; }

        public DateTime? CreatedDate { get; set; }
        public DateTime? EditedDate { get; set; }
    }
}