namespace Domain.Services.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Stories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Body = c.String(nullable: false),
                        Published = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(),
                        EditedDate = c.DateTime(),
                        Place_Id = c.Int(nullable: false),
                        Creator_Id = c.Int(),
                        Editor_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Places", t => t.Place_Id)
                .ForeignKey("dbo.Users", t => t.Creator_Id)
                .ForeignKey("dbo.Users", t => t.Editor_Id)
                .Index(t => t.Place_Id)
                .Index(t => t.Creator_Id)
                .Index(t => t.Editor_Id);
            
            CreateTable(
                "dbo.Places",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Location = c.Geography(nullable: false),
                        Name = c.String(),
                        CreatedDate = c.DateTime(),
                        EditedDate = c.DateTime(),
                        Creator_Id = c.Int(),
                        Editor_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Creator_Id)
                .ForeignKey("dbo.Users", t => t.Editor_Id)
                .Index(t => t.Creator_Id)
                .Index(t => t.Editor_Id);
            
            CreateTable(
                "dbo.Keywords",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Published = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(),
                        EditedDate = c.DateTime(),
                        Creator_Id = c.Int(),
                        Editor_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.Creator_Id)
                .ForeignKey("dbo.Users", t => t.Editor_Id)
                .Index(t => t.Creator_Id)
                .Index(t => t.Editor_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FacebookId = c.Int(nullable: false),
                        Username = c.String(),
                        Email = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Description = c.String(),
                        CreatedDate = c.DateTime(),
                        EditedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Multimedias",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Extension = c.String(nullable: false),
                        Size = c.Int(nullable: false),
                        Published = c.Boolean(nullable: false),
                        CreatedDate = c.DateTime(),
                        EditedDate = c.DateTime(),
                        Story_Id = c.Int(nullable: false),
                        Creator_Id = c.Int(),
                        Editor_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Stories", t => t.Story_Id, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.Creator_Id)
                .ForeignKey("dbo.Users", t => t.Editor_Id)
                .Index(t => t.Story_Id)
                .Index(t => t.Creator_Id)
                .Index(t => t.Editor_Id);
            
            CreateTable(
                "dbo.UserJoinKeyword",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        KeywordId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.KeywordId })
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.Keywords", t => t.KeywordId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.KeywordId);
            
            CreateTable(
                "dbo.MultimediaJoinKeyword",
                c => new
                    {
                        MultimediaId = c.Int(nullable: false),
                        KeywordId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.MultimediaId, t.KeywordId })
                .ForeignKey("dbo.Multimedias", t => t.MultimediaId, cascadeDelete: true)
                .ForeignKey("dbo.Keywords", t => t.KeywordId, cascadeDelete: true)
                .Index(t => t.MultimediaId)
                .Index(t => t.KeywordId);
            
            CreateTable(
                "dbo.PlaceJoinKeyword",
                c => new
                    {
                        PlaceId = c.Int(nullable: false),
                        KeywordId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PlaceId, t.KeywordId })
                .ForeignKey("dbo.Places", t => t.PlaceId, cascadeDelete: true)
                .ForeignKey("dbo.Keywords", t => t.KeywordId, cascadeDelete: true)
                .Index(t => t.PlaceId)
                .Index(t => t.KeywordId);
            
            CreateTable(
                "dbo.StoryJoinKeyword",
                c => new
                    {
                        StoryId = c.Int(nullable: false),
                        KeywordId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.StoryId, t.KeywordId })
                .ForeignKey("dbo.Stories", t => t.StoryId, cascadeDelete: true)
                .ForeignKey("dbo.Keywords", t => t.KeywordId, cascadeDelete: true)
                .Index(t => t.StoryId)
                .Index(t => t.KeywordId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.StoryJoinKeyword", new[] { "KeywordId" });
            DropIndex("dbo.StoryJoinKeyword", new[] { "StoryId" });
            DropIndex("dbo.PlaceJoinKeyword", new[] { "KeywordId" });
            DropIndex("dbo.PlaceJoinKeyword", new[] { "PlaceId" });
            DropIndex("dbo.MultimediaJoinKeyword", new[] { "KeywordId" });
            DropIndex("dbo.MultimediaJoinKeyword", new[] { "MultimediaId" });
            DropIndex("dbo.UserJoinKeyword", new[] { "KeywordId" });
            DropIndex("dbo.UserJoinKeyword", new[] { "UserId" });
            DropIndex("dbo.Multimedias", new[] { "Editor_Id" });
            DropIndex("dbo.Multimedias", new[] { "Creator_Id" });
            DropIndex("dbo.Multimedias", new[] { "Story_Id" });
            DropIndex("dbo.Keywords", new[] { "Editor_Id" });
            DropIndex("dbo.Keywords", new[] { "Creator_Id" });
            DropIndex("dbo.Places", new[] { "Editor_Id" });
            DropIndex("dbo.Places", new[] { "Creator_Id" });
            DropIndex("dbo.Stories", new[] { "Editor_Id" });
            DropIndex("dbo.Stories", new[] { "Creator_Id" });
            DropIndex("dbo.Stories", new[] { "Place_Id" });
            DropForeignKey("dbo.StoryJoinKeyword", "KeywordId", "dbo.Keywords");
            DropForeignKey("dbo.StoryJoinKeyword", "StoryId", "dbo.Stories");
            DropForeignKey("dbo.PlaceJoinKeyword", "KeywordId", "dbo.Keywords");
            DropForeignKey("dbo.PlaceJoinKeyword", "PlaceId", "dbo.Places");
            DropForeignKey("dbo.MultimediaJoinKeyword", "KeywordId", "dbo.Keywords");
            DropForeignKey("dbo.MultimediaJoinKeyword", "MultimediaId", "dbo.Multimedias");
            DropForeignKey("dbo.UserJoinKeyword", "KeywordId", "dbo.Keywords");
            DropForeignKey("dbo.UserJoinKeyword", "UserId", "dbo.Users");
            DropForeignKey("dbo.Multimedias", "Editor_Id", "dbo.Users");
            DropForeignKey("dbo.Multimedias", "Creator_Id", "dbo.Users");
            DropForeignKey("dbo.Multimedias", "Story_Id", "dbo.Stories");
            DropForeignKey("dbo.Keywords", "Editor_Id", "dbo.Users");
            DropForeignKey("dbo.Keywords", "Creator_Id", "dbo.Users");
            DropForeignKey("dbo.Places", "Editor_Id", "dbo.Users");
            DropForeignKey("dbo.Places", "Creator_Id", "dbo.Users");
            DropForeignKey("dbo.Stories", "Editor_Id", "dbo.Users");
            DropForeignKey("dbo.Stories", "Creator_Id", "dbo.Users");
            DropForeignKey("dbo.Stories", "Place_Id", "dbo.Places");
            DropTable("dbo.StoryJoinKeyword");
            DropTable("dbo.PlaceJoinKeyword");
            DropTable("dbo.MultimediaJoinKeyword");
            DropTable("dbo.UserJoinKeyword");
            DropTable("dbo.Multimedias");
            DropTable("dbo.Users");
            DropTable("dbo.Keywords");
            DropTable("dbo.Places");
            DropTable("dbo.Stories");
        }
    }
}
