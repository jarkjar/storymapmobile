﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;

namespace Domain.Services
{
    public enum CacheDuration
    {
        Default = 30,
        Keywords = 30,
        Stories = 30,
        Places = 30
    }

    public class StoryMapCache<T> where T : class
    {
        public string Key { get; private set; }

        public StoryMapCache(string key)
        {
            this.Key = key;
        }

        public T Get()
        {
            if (HttpContext.Current == null)
                return null;

            object data = HttpContext.Current.Cache.Get(this.Key.ToString());
            if (data == null)
                return null;

            return data as T;
        }

        /// <summary>
        /// Set data to cache
        /// </summary>
        /// <param name="data"></param>
        public void Set(T data, int duration)
        {
            bool useCaching = true;
#if DEBUG
            useCaching = true;
#endif
            if (duration > 0 && useCaching && this.Key != null && data != null && HttpContext.Current != null)
                HttpContext.Current.Cache.Insert(this.Key.ToString(), data, null, DateTime.Now.AddSeconds((int)duration), Cache.NoSlidingExpiration);
        }
    }
}
