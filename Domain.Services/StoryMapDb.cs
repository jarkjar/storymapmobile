﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Domain.Entities;
using Domain.Services.Migrations;

namespace Domain.Services
{
    public class StoryMapDb : DbContext
    {
        public DbSet<Story> Stories { get; set; }
        public DbSet<Place> Places { get; set; }
        public DbSet<Keyword> Keywords { get; set; }
        public DbSet<Multimedia> Multimedia { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Story <-> Keyword
            modelBuilder.Entity<Story>()
                .HasMany(s => s.Keywords)
                .WithMany(k => k.Stories)
                .Map(mc =>
                {
                    mc.MapLeftKey("StoryId");
                    mc.MapRightKey("KeywordId");
                    mc.ToTable("StoryJoinKeyword");
                });

            // User <-> Keyword
            modelBuilder.Entity<User>()
                .HasMany(s => s.InterestedKeywords)
                .WithMany(k => k.UsersWhoAreInterested)
                .Map(mc =>
                {
                    mc.MapLeftKey("UserId");
                    mc.MapRightKey("KeywordId");
                    mc.ToTable("UserJoinKeyword");
                });

            // Place <-> Keyword
            modelBuilder.Entity<Place>()
                .HasMany(s => s.Keywords)
                .WithMany(k => k.Places)
                .Map(mc =>
                {
                    mc.MapLeftKey("PlaceId");
                    mc.MapRightKey("KeywordId");
                    mc.ToTable("PlaceJoinKeyword");
                });

            // Place <-> Story
            modelBuilder.Entity<Place>()
                .HasMany(s => s.Stories)
                .WithRequired(k => k.Place)
                .WillCascadeOnDelete(false);

            // Multimedia <-> Keyword
            modelBuilder.Entity<Multimedia>()
                .HasMany(s => s.Keywords)
                .WithMany(k => k.Multimedia)
                .Map(mc =>
                {
                    mc.MapLeftKey("MultimediaId");
                    mc.MapRightKey("KeywordId");
                    mc.ToTable("MultimediaJoinKeyword");
                });
        }
    }
}