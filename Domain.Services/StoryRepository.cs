﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Entities;
using System.Data.Entity;
using System.Data.Spatial;
using System.Globalization;

namespace Domain.Services
{
    public class StoryRepository
    {
        int keywordCacheDuration = 60;
        int storiesCacheDuration = 120;
        int placesCacheDuration = 60;

        public StoryMapDb dbContext = new StoryMapDb();

        /// <summary>
        /// Return all keywords by published state
        /// </summary>
        /// <returns></returns>
        public IQueryable<Keyword> GetAllKeywords(bool published = true)
        {
            try
            {
                var key = published ? "Cache_AllKeywordsPublished" : "Cache_AllKeywordsNotPublished";
                var cache = new StoryMapCache<IQueryable<Keyword>>(key);
                var keywords = cache.Get();
                if (keywords != null)
                    return keywords;

                keywords = this.dbContext.Keywords.ToList().AsQueryable()
                    .Where(keyword => keyword.Published == published);
                cache.Set(keywords, keywordCacheDuration);
                return keywords;
            }
            catch (Exception)
            {
            }
            return Enumerable.Empty<Keyword>().AsQueryable();
        }

        /// <summary>
        /// Return all places where are published stories
        /// </summary>
        /// <returns></returns>
        public IQueryable<Place> GetAllPlaces()
        {
            try
            {
                var key = "Cache_AllPlaces";
                var cache = new StoryMapCache<IQueryable<Place>>(key);
                var places = cache.Get();
                if (places != null)
                    return places;

                places = this.dbContext.Places.ToList().AsQueryable()
                    .Include("Keywords").Include("Stories")
                    .Include(p => p.Stories.Select(s => s.Keywords))
                    .Where(p => p.Stories.Any(s => s.Published));
                cache.Set(places, placesCacheDuration);
                return places;
            }
            catch (Exception)
            {
            }
            return Enumerable.Empty<Place>().AsQueryable();
        }

        /// <summary>
        /// Get all published stories
        /// </summary>
        /// <returns></returns>
        public IQueryable<Story> GetAllStories()
        {
            try
            {
                var key = "Cache_AllStoriesPublished";
                var cache = new StoryMapCache<IQueryable<Story>>(key);
                var stories = cache.Get();
                if (stories != null)
                    return stories;
                stories = this.dbContext.Stories.ToList().AsQueryable()
                    .Include("Place").Include(s => s.Place.Keywords)
                    .Include("Keywords").Include("Editor").Include("Creator")
                    .Where(s => s.Published);
                cache.Set(stories, storiesCacheDuration);
                return stories;
            }
            catch (Exception)
            {
            }
            return Enumerable.Empty<Story>().AsQueryable();
        }
        
        public bool CreateStory(string placeName, double lat, double lng, string title, string body, string rawKeywords, bool published)
        {
            int saved = 0;
            try
            {
                var place = new Place()
                {
                    CreatedDate = DateTime.Now,
                    Creator = null,
                    EditedDate = null,
                    Editor = null,
                    Location = ConvertToDbGeography(lat, lng),
                    Name = placeName
                };

                var keywords = ParseKeywords(rawKeywords);
                place.Keywords = keywords;

                var story = new Story()
                {
                    Title = title,
                    Body = body,
                    Place = place,
                    Keywords = keywords,
                    CreatedDate = DateTime.Now,
                    EditedDate = null,
                    Creator = null,
                    Editor = null,
                    Published = published
                };
                dbContext.Stories.Add(story);
                saved = dbContext.SaveChanges();
            }
            catch (Exception)
            {
            }
            return saved > 0;
        }

        public DbGeography ConvertToDbGeography(double lat, double lng)
        {
            return DbGeography.FromText(string.Format(new CultureInfo("en-US"), "POINT({0} {1})", lng, lat));
        }

        /// <summary>
        /// Parse keyword entities from raw string input e.g. " auto, engine ,power , , "
        /// </summary>
        /// <param name="rawKeywords"></param>
        /// <returns></returns>
        private ICollection<Keyword> ParseKeywords(string rawKeywords)
        {
            // Remove white spaces from start and end and empty entries then select distinct words
            var tagCollection = rawKeywords
                .Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(tag => tag.Trim())
                .Where(tag => !string.IsNullOrEmpty(tag))
                .Select(tag => tag)
                .Distinct(StringComparer.InvariantCultureIgnoreCase);

            var publishedKeywords = this.GetAllKeywords().AsEnumerable();
            var existsKeywords = from Keyword keyword in publishedKeywords
                                 where tagCollection.Contains(keyword.Name, StringComparer.InvariantCultureIgnoreCase)
                                 select keyword;

            // Remove banned keywords from new keywords
            var bannedKeywords = this.GetAllKeywords(false).AsEnumerable();
            var newKeywords = from string name in tagCollection
                              where !publishedKeywords.Select(k => k.Name).Contains(name, StringComparer.InvariantCultureIgnoreCase) &&
                                    !bannedKeywords.Select(k => k.Name).Contains(name, StringComparer.InvariantCultureIgnoreCase)
                              select new Keyword()
                              {
                                  Name = name,
                                  CreatedDate = DateTime.Now,
                                  Creator = null,
                                  EditedDate = null,
                                  Editor = null,
                                  Published = true
                              };

            var keywords = new List<Keyword>();
            keywords.AddRange(existsKeywords);
            keywords.AddRange(newKeywords);
            return keywords;
        }

        public Story GetStoryByTitle(string title)
        {
            Story story = null;
            try
            {
                var query = from i in dbContext.Stories
                            where i.Title == title
                            select i;
                story = query.FirstOrDefault();
            }
            catch (Exception)
            {
            }
            return story;
        }
    }
}
