﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="StoryMap.Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html>
<html>
<head>
    <title style="font-family: 'Agency FB'">Confestigate</title>
    <meta name="viewport" content="width=device-width; height=device-height; initial-scale=1.0; user-scalable=yes">
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCZS4x5UHV9n4TMMfbn--2DxJYi-RQEZM8&libraries=places&sensor=false"></script>
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
    <link rel="stylesheet" href="css/main.css" />
    <script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
    <script src="http://code.jquery.com/ui/1.9.1/jquery-ui.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
    <script type="text/javascript" src="scripts/googlemaps.js?v=2"></script>
</head>
<body>
    <form id="form1" runat="server">
        <!-- default page -->
        <div data-role="page" id="default">
            <script>
                $.mobile.ajaxEnabled = false;
            </script>
            <div data-role="header" class="ui-bar" data-nobackbtn="true">
                <div data-role="navbar">
                    <ul>
                        <li><a id="btnNearStories" href="ShowStories\near" data-role="button">Story</a></li>
                        <li><a href="#default" class="ui-btn-active ui-state-persist">Map</a></li>
                    </ul>
                </div>
            </div>
            <asp:Panel runat="server" ID="pnlInfoMessage" Visible="false">
                <div align="center" id="infoMessage" runat="server">
                    <asp:Label runat="server" ID="lblInfo" />
                </div>
                <script type="text/javascript">
                    $(function () {
                        $("#infoMessage").delay(3000).fadeOut(3000, function () { $(this).remove(); });
                    });
                </script>
            </asp:Panel>
            <div data-role="content">
                <div id="searchPanel">
                    <asp:TextBox ID="tbSearchTextField" runat="server" placeholder="Search location e.g. street or city" />
                    <!--<form action="php/formtarget.php" method="post" id="tagTextField">
                    <input type="text" name="tag[]" value="" class="tag"/>
                </form>-->
                </div>
                <div id="map_canvas"></div>
            </div>
            <div data-role="footer" data-position="fixed">
                <div data-role="navbar">
                    <ul>
                        <li><a href="#about" data-role="button" data-rel="dialog" data-icon="info">About</a></li>
                        <li><a href="#settings" data-role="button" data-ajax="false" data-icon="gear">Settings</a></li>
                        <li><a id="btnCreate" href="Create" data-role="button" data-ajax="false" data-iconpos="right" data-icon="plus">New</a></li>
                    </ul>
                </div>
                <asp:LoginView runat="server" Visible="false">
                    <AnonymousTemplate>
                        <%--<a id="registerLink" runat="server" data-rel="dialog" data-ajax="false" href="Register">Register</a>--%>
                        <%--<a id="loginLink" runat="server" data-rel="dialog" data-ajax="false" href="Login">Login</a>--%>
                    </AnonymousTemplate>
                    <LoggedInTemplate>
                        <a runat="server" href="Manage" data-ajax="false" data-rel="dialog">
                            <%--<asp:LoginName runat="server" />--%>
                            Account
                        </a>
                        <asp:LoginStatus runat="server" LogoutAction="Redirect" LogoutText="Logout" LogoutPageUrl="~/" />
                    </LoggedInTemplate>
                </asp:LoginView>
            </div>
        </div>
        <!-- end of default page -->

        <!-- settings page -->
        <div data-role="dialog" id="settings">
            <div data-role="header">
                <h1>Settings</h1>
            </div>
            <div data-role="content" data-theme="a" data-inset="true">
                <fieldset>
                    <legend></legend>
                    <div data-role="fieldcontain">
                        <label for="inputRadius">Radius (meters):</label>
                        <input type="range" id="inputRadius" name="inputRadius" data-theme="a" data-track-theme="b" min="10" step="10" max="10000" value="500">
                    </div>
                    <%--<div>
                        Tags e.g. Your interests:
                        <input type="search" id="inputFilterTags" name="inputFilterTags">
                    </div>--%>
                    <div data-role="fieldcontain">
                        <label for="cbWatchUserPosition">Watch your location</label>
                        <select name="cbWatchUserPosition" id="cbWatchUserPosition" data-role="slider" data-theme="a" onchange="setWatchUserPosition(this);">
                            <option value="off">Off</option>
                            <option value="on">On</option>
                        </select>
                    </div>
                    <div>
                        <!-- refreshSettings method updates link paramters based to settings -->
                        <a href="#default" data-role="button" data-rel="back" data-icon="check" data-theme="b"
                            onclick="refreshSettings();">Save</a>
                    </div>
                </fieldset>
            </div>
        </div>
        <!-- end of settings page -->

        <!-- about page -->
        <div data-role="dialog" id="about">
            <div data-role="header">
                <h1>About</h1>
            </div>
            <div data-role="content" data-theme="a" data-inset="true">
                <fieldset>
                    <legend></legend>
                    <div>
                        <span style="font-size: larger">Business: </span><span style="font-family: 'Courier New'">Jussi and Aleksi</span>
                    </div>
                    <div>
                        <span style="font-size: larger">Code: </span><span style="font-family: 'Courier New'">Sakari, Jarkko, Miika</span>
                    </div>
                    <a href="#default" data-role="button" data-rel="back" data-theme="b">
                        <span class="ui-btn-text">Hey nice!</span>
                    </a>
                </fieldset>
            </div>
            <div data-role="footer">
                <h4>Surffitiimi / Lean Software Startup 2012</h4>
            </div>
        </div>
        <!-- end of about page -->
    </form>

    <!-- Hidden fields to store information -->
    <input type="hidden" id="latitude" name="latitude" />
    <input type="hidden" id="longitude" name="longitude" />
    <input type="hidden" id="altitude" name="altitude" />
    <input type="hidden" id="accuracy" name="accuracy" />
    <input type="hidden" id="altitudeAccuracy" name="altitudeAccuracy" />
    <input type="hidden" id="heading" name="heading" />
    <input type="hidden" id="speed" name="speed" />
    <input type="hidden" id="placename" name="placename" />
</body>
</html>
