﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateStory.aspx.cs" Inherits="StoryMap.CreateStory" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html>

<html>
<head runat="server">
    <title style="font-family: 'Agency FB'">Confestigate</title>
    <meta name="viewport" content="width=device-width; height=device-height; initial-scale=1.0; user-scalable=yes">
    <%--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCZS4x5UHV9n4TMMfbn--2DxJYi-RQEZM8&libraries=places&sensor=false"></script>--%>
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
    <%--<link rel="StyleSheet" href="css/ui-lightness/jquery-ui-1.8.6.custom.css" type="text/css" media="all" />--%>
    <link rel="StyleSheet" href="css/jquery.tagedit.css" type="text/css" media="all" />
    <link rel="stylesheet" href="css/main.css" />
    <%--<link rel="stylesheet" href="css/tokeninput.css" />--%>

    <script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
    <script src="http://code.jquery.com/ui/1.9.1/jquery-ui.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
    <script type="text/javascript" src="scripts/jquery.autoGrowInput.js"></script>
    <script type="text/javascript" src="scripts/jquery.tokeninput.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnableScriptLocalization="true"
            EnableScriptGlobalization="true" ScriptMode="Release" />
        <ajaxToolkit:NoBot ID="noBot" runat="server" OnGenerateChallengeAndResponse="PageNoBot_GenerateChallengeAndResponse"
            ResponseMinimumDelaySeconds="2" CutoffWindowSeconds="10" CutoffMaximumInstances="5"></ajaxToolkit:NoBot>
        <asp:HiddenField ID="hdnLatitude" runat="server" />
        <asp:HiddenField ID="hdnLongitude" runat="server" />

        <!-- create page -->
        <div data-role="dialog" id="create" data-rel="back">
            <script>
                $.mobile.ajaxEnabled = false;
            </script>

            <div data-role="header">
                <h1>Create story</h1>
            </div>
            <div data-role="content" data-theme="a" data-inset="true">
                <fieldset>
                    <legend></legend>
                    <asp:Label ID="lblTitle" Text="Title" runat="server" />
                    <asp:RequiredFieldValidator ID="rfvTitle" ControlToValidate="tbTitle" runat="server"
                        Display="Static" ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="CreateStory" ForeColor="Red" />
                    <asp:TextBox ID="tbTitle" runat="server" />

                    <asp:Label ID="lblStory" Text="Story" runat="server" />
                    <asp:RequiredFieldValidator ID="rfvStory" ControlToValidate="tbStory" runat="server"
                        Display="Static" ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="CreateStory" ForeColor="Red" />
                    <asp:TextBox ID="tbStory" runat="server" TextMode="MultiLine" Columns="30" Rows="5" />

                    <asp:Label ID="lblPlace" Text="Place name:" runat="server" />
                    <asp:RequiredFieldValidator ID="rfvPlace" ControlToValidate="tbPlace" runat="server"
                        Display="Static" ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="CreateStory" ForeColor="Red" />
                    <asp:TextBox ID="tbPlace" runat="server" />

                    <asp:Label ID="lblTags" Text="Keywords (Comma separate e.g. Auto, Audi):" runat="server" />
                    <asp:RequiredFieldValidator ID="rfvTags" ControlToValidate="tbTags" runat="server"
                        Display="Static" ErrorMessage="Required" SetFocusOnError="true" ValidationGroup="CreateStory" ForeColor="Red" />
                    <asp:TextBox ID="tbTags" runat="server" />
                    <%--<script type="text/javascript">
                        $(document).ready(function () {
                            $("#tbTags").tokenInput("api/v2/Keywords?$format=json", {
                                minChars: 2,
                                tokenLimit: 5,
                                resultsLimit: 5,
                                propertyToSearch: "Name",
                                jsonContainer: "value",
                                theme: "facebook",
                                preventDuplicates: true,
                                tokenValue: name
                            });
                        });
                    </script>--%>
                    <p>
                        By submitting this information, I acknowledge that I have read and agree to the 
                        <a href="#">Terms of Use</a> and <a href="#">Privacy Policy</a>
                    </p>
                    <asp:Button ID="btnCreateStory" runat="server" Text="Save" ValidationGroup="CreateStory"
                        OnClick="btnCreateStory_Click" data-role="button" data-icon="check" data-theme="b" />
                </fieldset>
            </div>
        </div>
        <!-- end of create page -->
    </form>
</body>
</html>
