﻿// Updated 11.12.2012
var geocoder;
var mapOptions;
var map;
var currentUserMarker;
var currentPosition;
var infoWindow;

// Default zoom value. Used when page loaded
var defaultZoom = 12;
// Default position (Turku, ICT-talo)
var defaultCoords = {
    coords: {
        latitude: 60.449249,
        longitude: 22.259239
    }
};
var currentPositionPinColor = "FE7569";
var otherPositionPinColor = "7569FE";
var pinShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
        new google.maps.Size(40, 37),
        new google.maps.Point(0, 0),
        new google.maps.Point(12, 35));

// Contains all STORY markers (not current location marker)
var markerArray = [];
var cookieExpireTimeMinutes = 5;
var watchId;
var interval;

var cache = {};
var cacheKey = 'storymap';


/// FUNCTIONS

function runPlaceQueryJSON() {
    // TODO: Use bounds in query
    //var bounds = map.getBounds();
    if (cache[cacheKey]) {
        refreshMarkers(cache[cacheKey]);
    }
    else {
        // Cache has been expired
        $.ajax({
            url: "api/v2/Places?$expand=Stories&$format=json",
            success: function (data, msg) {
                cache[cacheKey] = data;
                refreshMarkers(data);
            },
            dataType: "json",
            timeout: 30000
        });
    }
}

$(document).ready(function () {
    initialize();

    var cd = getQuerystring("cd");
    if (cd == "1") {
        cache = {};
        runPlaceQueryJSON();
        getCurrentPosition(true);
    }
    else
        getCurrentPosition();
    
    // Clear cache every 60s
    interval = setInterval(function () {
        cache = {};
    }, 60000);
});

// Initializes variables and map
function initialize() {
    // Set map size by user agent
    var useragent = navigator.userAgent;
    var canvas = document.getElementById('map_canvas');
    if (useragent.indexOf('iPhone') != -1 || useragent.indexOf('Android') != -1) {
        canvas.style.width = '100%';
        canvas.style.height = '100%';
    }

    mapOptions = {
        center: new google.maps.LatLng(defaultCoords.coords.latitude, defaultCoords.coords.longitude),
        zoom: defaultZoom,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(canvas, mapOptions);
    geocoder = new google.maps.Geocoder();

    //set the minimum height of the map
    var minSize = 300;
    //cache reference to jQuery vars we need in the resize method
    var $map = $("#map_canvas");
    var $win = $(window);
    //start out by setting the height
    $map.height($win.height() - 120);
    $(window).resize(function () {
        //make sure the new size is at least the min-height we want
        var newSize = $win.height() - 120;
        if (parseInt(newSize, 10) < minSize) {
            newSize = minSize;
        }
        $map.height(newSize);
    });

    // Set auto complete to search field
    var input = document.getElementById('tbSearchTextField');
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

    // If place changed then show users new position
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();
        $(input).removeClass('notfound');
        if (!place.geometry) {
            // Inform the user that the place was not found and return.
            $(input).addClass('notfound');
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(defaultZoom);
        }
        showUserLocation(place.geometry.location);
    });

    // make it execute a function called runPlaceQueryJSON when the map stops panning.
    google.maps.event.addListener(map, 'idle', runPlaceQueryJSON);
}

function getQuerystring(key, default_) {
    if (default_ == null) default_ = "";
    key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
    var qs = regex.exec(window.location.href);
    if (qs == null)
        return default_;
    else
        return qs[1];
}

function getCurrentPosition(showDefaultPosition) {
    // Check if recent marker position has been set in cookie
    if (showDefaultPosition)
        getUserLocationFromCookie();
    else if (navigator.geolocation && !showDefaultPosition) {
        // timeout: milliseconds
        // maximumAge: We accept positions whose age is not greater than 10 minutes. 
        // If the user agent does not have a fresh enough cached position object, 
        // it will automatically acquire a new one.
        var options = { timeout: 5000, maximumAge: 600000, enableHighAccuracy: true };
        navigator.geolocation.getCurrentPosition(showUserLocation, initErrorHandler, options);
    }
    else {
        // Show default location
        showUserLocation(defaultCoords);
    }
}

function setWatchUserPosition(sel) {
    if (sel.value == "on")
        watchUserPosition();
    else
        stopUserPositionWatch();
}

function watchUserPosition() {
    if (navigator.geolocation) {
        watchId = navigator.geolocation.watchPosition(watchSuccess, initErrorHandler);
    }
}

function watchSuccess(position) {
    var latlan = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    showUserLocation(latlan);
}

function stopUserPositionWatch() {
    if (navigator.geolocation) {
        navigator.geolocation.clearWatch(watchId);
    }
}

function getUserLocationFromCookie() {
    // Check if recent marker position has been set in cookie
    var recent_marker_lat = readCookie("rmlat");
    var recent_marker_lng = readCookie("rmlng");
    if (recent_marker_lat != null && recent_marker_lat != "" && recent_marker_lng != null && recent_marker_lng != "") {
        var recentCoords = {
            coords: {
                latitude: recent_marker_lat,
                longitude: recent_marker_lng
            }
        };
        showUserLocation(recentCoords);
    }
}

function initErrorHandler(err) {
    switch (err.code) {
        case err.TIMEOUT:
            getCurrentPosition(true);
            break;
    }
    if (err.code == 1) {
        alert('Error: Access is denied!');
    } else if (err.code == 2) {
        alert('Error: Position is unavailable!');
    }
}

function refreshMarkers(data) {
    // Clear markers
    if (markerArray) {
        $.each(markerArray, function (index, marker) {
            marker.setMap(null);
        });
    }
    markerArray = [];

    // Set markers
    $.each(data.value, function (index, place) {
        if (!place.Stories)
            return;
        var count = place.Stories.length;
        if (count > 0) {
            var position = new google.maps.LatLng(place.Latitude, place.Longitude);
            var body;
            var title;
            var ids;
            if (count > 1) {
                title = place.Name;
                body = count + ' stories available';
                $.each(place.Stories, function (index, story) {
                    ids += ';' + story.Id; // Separate story ids with semicolon
                });
            }
            else {
                title = place.Stories[0].Title;
                body = place.Stories[0].Body;
                ids = place.Stories[0].Id;
            }
            var markerPosition = {
                coords: {
                    latitude: place.Latitude,
                    longitude: place.Longitude
                }
            };
            markerArray.push(createMarker(markerPosition, title, body, ids));
        }
    });
}

function showUserLocation(location) {
    if (!currentUserMarker)
        currentUserMarker = createMarker(location, 'Current position', 'You are here', null, true, currentPositionPinColor);
    else if (location.coords) {
        var latlan = new google.maps.LatLng(location.coords.latitude, location.coords.longitude);
        currentUserMarker.setPosition(latlan);
    } else {
        currentUserMarker.setPosition(location);
    }

    // Store data for later usage
    //$('#latitude').val(coords.latitude);
    //$('#longitude').val(coords.longitude);
    //$('#altitude').val(coords.altitude);
    //$('#accuracy').val(coords.accuracy);
    //$('#altitudeAccuracy').val(coords.altitudeAccuracy);
    //$('#heading').val(coords.heading);
    //$('#speed').val(coords.speed);

    // Set current location, center map to it and show address
    var currentPosition = currentUserMarker.getPosition();
    map.setCenter(currentPosition);
    showAddress(currentPosition);

    //Add listener to marker for reverse geocoding
    google.maps.event.addListener(currentUserMarker, 'dragend', function () {
        var markerPosition = currentUserMarker.getPosition();
        showAddress(markerPosition);
    });
}

function refreshSettings() {
    if (currentUserMarker) {
        var currentPosition = currentUserMarker.getPosition();
        // Find stories near by current location, radius is in meters
        var radius = $('#inputRadius').val();
        if (!radius)
            radius = 10;
        $('#btnNearStories').attr('href',
            'ShowStories/near?lat=' + currentPosition.lat() + '&lng=' + currentPosition.lng() + '&rad=' + radius);
    }
}

function showAddress(currentPosition) {

    // Set cookies to show users recent place (even refresh page)
    createCookie('rmlat', currentPosition.lat(), cookieExpireTimeMinutes);
    createCookie('rmlng', currentPosition.lng(), cookieExpireTimeMinutes);

    // Init create button navigation url
    $('#btnCreate').attr('href',
        'Create?lat=' + currentPosition.lat() + '&lng=' + currentPosition.lng());
    // Find stories near by current location, radius is in meters
    var radius = $('#inputRadius').val();
    if (!radius)
        radius = 10;
    $('#btnNearStories').attr('href',
        'ShowStories/near?lat=' + currentPosition.lat() + '&lng=' + currentPosition.lng() + '&rad=' + radius);

    var latlng = new google.maps.LatLng(currentPosition.lat(), currentPosition.lng());
    geocoder.geocode({ 'latLng': latlng }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[0]) {
                // Show address
                var name = results[0].formatted_address;
                $('#tbSearchTextField').val(name);
                // Complete create url
                $('#btnCreate').attr('href', function (i, val) {
                    return val + '&name=' + name;
                });
            }
        }
    });
}

// Create marker and return it. Sets also info window if body is not null
function createMarker(currentCoords, title, body, ids, draggable, color) {
    if (!color)
        color = otherPositionPinColor;

    var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + color,
        new google.maps.Size(21, 34),
        new google.maps.Point(0, 0),
        new google.maps.Point(10, 34));
    var pinMarker;
    if (ids) {
        pinMarker = new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng(currentCoords.coords.latitude, currentCoords.coords.longitude),
            title: title,
            draggable: draggable,
            icon: pinImage,
            shadow: pinShadow,
            url: './ShowStories/' + ids
        });

        google.maps.event.addListener(pinMarker, 'click', function () {
            window.location.href = pinMarker.url;
        });
    }
    else {
        pinMarker = new google.maps.Marker({
            map: map,
            position: new google.maps.LatLng(currentCoords.coords.latitude, currentCoords.coords.longitude),
            title: title,
            draggable: draggable,
            icon: pinImage,
            shadow: pinShadow,
        });
    }

    //if (body) {
    //    var html = '<div class="markerInfoBody">' + body + '</div>';
    //    google.maps.event.addListener(pinMarker, "click", function () {
    //        if (infoWindow)
    //            infoWindow.close();
    //        infoWindow = new google.maps.InfoWindow({
    //            content: body
    //        });
    //        infoWindow.setContent(html);
    //        infoWindow.open(map, pinMarker);
    //    });
    //}
    return pinMarker;
}


// COOKIE HANDLERS

function createCookie(name, value, minutes) {
    var exdate = new Date();
    exdate.setMinutes(exdate.getMinutes() + minutes);
    document.cookie = name + '=' + value + ';expires=' + exdate.toUTCString();
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0)
            return c.substring(nameEQ.length, c.length);
    }
    return null;
}
