﻿$(document).ready(function () {
    $('#tagTextField').find('input.tag').tagedit({
        allowEdit: false,
        autocompleteOptions: {
            source: function (request, response) {
                $.ajax({
                    url: "/api/v1/Keywords",
                    dataType: "json",
                    success: function(data) {
                        response($.map(data.Keywords, function(elementOfArray, indexInArray) { return elementOfArray.Name; }));
                    }
                });
            }
        }
    });
});