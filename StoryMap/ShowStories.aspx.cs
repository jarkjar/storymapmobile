﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Domain.Entities;
using Domain.Services;
using System.Data.Spatial;
using System.Globalization;

namespace StoryMap
{
    public partial class ShowStories : System.Web.UI.Page
    {
        StoryRepository repository = new StoryRepository();
        List<UiStory> UiStories
        {
            get
            {
                return ViewState["UiStories"] as List<UiStory>;
            }
            set
            {
                ViewState["UiStories"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
                return;

            string data = Page.RouteData.Values["story"] as string;
            if (string.IsNullOrEmpty(data))
                return;

            if (data.ToLower() != "near")
            {
                var storyIds = ToIntArray(data);
                if (storyIds.Count() > 0)
                    ShowStoriesById(storyIds);
            }
            else if (data.ToLower() == "near")
            {
                var tagIds = ToIntArray(Request.QueryString["tag"] as string);
                double lat, lng, rad;
                if (double.TryParse(Request.QueryString["lat"], NumberStyles.Any, CultureInfo.InvariantCulture, out lat) &&
                    double.TryParse(Request.QueryString["lng"], NumberStyles.Any, CultureInfo.InvariantCulture, out lng) &&
                    double.TryParse(Request.QueryString["rad"], NumberStyles.Any, CultureInfo.InvariantCulture, out rad))
                {
                    if (rad > 10000)
                        rad = 10000;
                    else if (rad < 10)
                        rad = 10;
                    ShowFilteredStories(lat, lng, rad, tagIds);
                }
            }
        }

        private IEnumerable<int> ToIntArray(string data)
        {
            if (data == null)
                return Enumerable.Empty<int>();
            var separators = new string[] { ";" };
            int id;
            var storyIds = data.Split(separators, StringSplitOptions.RemoveEmptyEntries)
                .Select(s => int.TryParse(s, out id) ? id : 0).AsEnumerable();
            return storyIds;
        }

        private void ShowStoriesById(IEnumerable<int> storyIds)
        {
            var stories = repository.GetAllStories().Where(s => storyIds.Contains(s.Id)).AsEnumerable();
            UiStories = (from Story story in stories
                         select TransformStory(story)).ToList();

            DataBindListViews();
        }

        /// <summary>
        /// When calling this page e.g.
        /// /ShowStories/near?lat=60.385970584113274&lng=22.388515359863277&rad=1000&tag=1;2;3
        /// then this method filters stories by rad parameter based to current location (lat/lng) and 
        /// tag Ids (if exists)
        /// </summary>
        /// <param name="lat"></param>
        /// <param name="lng"></param>
        /// <param name="rad"></param>
        /// <param name="tagIds"></param>
        private void ShowFilteredStories(double lat, double lng, double rad, IEnumerable<int> tagIds)
        {
            using (var dbContext = new StoryMapDb())
            {
                bool tagFilterEnabled = tagIds.Count() > 0;
                var currentLocation = repository.ConvertToDbGeography(lat, lng);
                UiStories = (from Story story in repository.GetAllStories()
                             where // case when tags entered
                                   ((tagFilterEnabled && tagIds.Intersect(story.Keywords.Select(k => k.Id)).Any()) &&
                                   story.Place.Location.Distance(currentLocation) <= rad) ||
                                 // Case when no tags entered
                                   (!tagFilterEnabled && story.Place.Location.Distance(currentLocation) <= rad)
                             select TransformStory(story)).ToList();

                DataBindListViews();
            }
        }

        private void DataBindListViews()
        {
            lvStories.DataSource = UiStories;
            lvStories.SelectedIndex = 0;
            lvStories.DataBind();

            var keywords = UiStories.SelectMany(s => s.Keywords);
            var distinctKeywords = from keyword in keywords
                                   group keyword by new { keyword.Id }
                                       into keywordGroup
                                       select keywordGroup.First();
            lvTags.DataSource = distinctKeywords;
            lvTags.DataBind();

            var pnlRemoveFilter = lvTags.FindControl("pnlRemoveFilter") as Panel;
            if (pnlRemoveFilter != null)
                pnlRemoveFilter.Visible = UiStories.Count() > 1;
        }

        private UiStory TransformStory(Story story)
        {
            var uiStory = new UiStory
            {
                Id = story.Id,
                Title = story.Title,
                Body = story.Body,
                Creator = story.Creator != null ? story.Creator.Username : string.Empty,
                CreatedDate = story.CreatedDate,
                Latitude = story.Place.Location.Latitude.HasValue ? story.Place.Location.Latitude.Value : 0,
                Longitude = story.Place.Location.Longitude.HasValue ? story.Place.Location.Longitude.Value : 0,
                MapUrl = story.Place.Location.Latitude.HasValue && story.Place.Location.Longitude.HasValue ?
                    string.Format("./?lat={0}&lng={1}", story.Place.Location.Latitude.Value,
                    story.Place.Location.Longitude.Value).Replace(",", ".") : string.Empty,
                Keywords = (from keyword in story.Keywords
                            select TransformKeyword(keyword)).ToList(),
                KeywordList = string.Join(", ", story.Keywords.Select(k => k.Name))
            };
            return uiStory;
        }

        private UiKeyword TransformKeyword(Keyword keyword)
        {
            var uiKeyword = new UiKeyword();
            uiKeyword.Id = keyword.Id;
            uiKeyword.Name = keyword.Name;
            return uiKeyword;
        }

        [Serializable()]
        public class UiKeyword
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }

        [Serializable()]
        public class UiStory
        {
            public int Id { get; set; }
            public string Title { get; set; }
            public string Body { get; set; }
            public string Creator { get; set; }
            public DateTime? CreatedDate { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
            public string MapUrl { get; set; }
            public List<UiKeyword> Keywords { get; set; }
            public string KeywordList { get; set; }
        }

        protected void btnMap_Click(object sender, EventArgs e)
        {
            Response.Redirect("../?cd=1");
        }

        protected void lvTags_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (UiStories == null || UiStories.Count() < 2)
                return;
            if (string.Equals(e.CommandName, "Filter"))
            {
                int id;
                if (!int.TryParse(e.CommandArgument as string, out id))
                    return;
                lvStories.DataSource = UiStories
                    .Where(s => s.Keywords.Any(k => k.Id == id))
                    .Select(s => s);
                lvStories.DataBind();
            }
            else if (string.Equals(e.CommandName, "Remove"))
            {
                lvStories.DataSource = UiStories;
                lvStories.DataBind();
            }
        }
    }
}