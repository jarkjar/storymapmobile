﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Routing;

namespace StoryMap.RestApi.v1
{
    public interface IRestHandler
    {
        RouteValueDictionary RouteData { get; set; }
    }
}
