﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml.Serialization;
using System.IO;

namespace StoryMap.RestApi.v1
{
    public class OutputHelper
    {
        private static Format GetFormat(HttpContext context)
        {
            string requestedFormat = context.Request.QueryString["format"];
            Format format = (requestedFormat != null && requestedFormat.Equals("xml", StringComparison.InvariantCultureIgnoreCase) ? Format.Xml : Format.JSON);
            return format;
        }

        public static void Output<T>(HttpContext context, T data)
        {
            var format = GetFormat(context);
            string output = Serialize<T>(data, format);

            string callback = context.Request.QueryString["callback"];

            if (format == Format.JSON && !string.IsNullOrWhiteSpace(callback))
            {
                output = callback + "(" + output + ");";
            }

            switch (format)
            {
                case Format.JSON:
                    context.Response.ContentType = "application/json";
                    break;
                case Format.Xml:
                    context.Response.ContentType = "text/xml";
                    break;
            }
            context.Response.Write(output);
        }

        private static string Serialize<T>(object item, Format format)
        {
            if (format == Format.JSON)
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                return serializer.Serialize(item);
            }
            else if (format == Format.Xml)
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                StringWriter writer = new StringWriter();
                serializer.Serialize(writer, item);
                return writer.ToString();
            }
            return string.Empty;
        }
    }

    public enum Format
    {
        JSON,
        Xml
    }
}