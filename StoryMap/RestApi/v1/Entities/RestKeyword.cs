﻿using System.Data.Services.Common;

namespace StoryMap.RestApi.v1.Entities
{
    [DataServiceKey("Name")]
    public class RestKeyword
    {
        public int Id { get; set; }
        public string Name { get; set; }
        //public ICollection<RestStory> Stories { get; set; }
        //public ICollection<RestUser> UsersWhoAreInterested { get; set; }
        //public ICollection<RestPlace> Places { get; set; }
        //public ICollection<RestMultimedia> Multimedia { get; set; }
        //public bool Published { get; set; }
        //public RestUser Creator { get; set; }
        //public RestUser Editor { get; set; }
        //public DateTime? CreatedDate { get; set; }
        //public DateTime? EditedDate { get; set; }
    }
}