﻿using System;
using System.Data.Services.Common;

namespace StoryMap.RestApi.v1.Entities
{
    [DataServiceKey("Id")]
    public class RestUser
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Description { get; set; }
        //public ICollection<RestKeyword> InterestedKeywords { get; set; }
        //public ICollection<User> FollowedUserIds { get; set; }

        public DateTime? CreatedDate { get; set; }
        public DateTime? EditedDate { get; set; }
    }
}