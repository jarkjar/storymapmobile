﻿using System.Collections.Generic;
using System.Data.Services.Common;

namespace StoryMap.RestApi.v1.Entities
{
    [DataServiceKey("Id")]
    public class RestPlace
    {
        public RestPlace()
        {
            Keywords = new List<RestKeyword>();
            Stories = new List<RestStory>();
        }

        public int Id { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Name { get; set; }
        public ICollection<RestKeyword> Keywords { get; set; }
        public ICollection<RestStory> Stories { get; set; }
        //public RestUser Creator { get; set; }
        //public DateTime? CreatedDate { get; set; }
        //public RestUser Editor { get; set; }
        //public DateTime? EditedDate { get; set; }
    }
}