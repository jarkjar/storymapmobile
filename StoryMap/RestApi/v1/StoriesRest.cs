﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Routing;
using System.Xml.Serialization;
using StoryMap.RestApi.v1.Entities;
using StoryMap.RestApi.v1.Repository;

namespace StoryMap.RestApi.v1
{
    public class StoriesRest : IHttpHandler, IRestHandler
    {
        public RouteValueDictionary RouteData { get; set; }

        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            var storiesRoot = new StoriesRoot();
            storiesRoot.Stories = AllStories;

            OutputHelper.Output<StoriesRoot>(context, storiesRoot);
        }

        [XmlRoot(ElementName = "Stories")]
        public class StoriesRoot
        {
            [XmlElement("Story")]
            public List<RestStory> Stories { get; set; }
        }

        public static List<RestStory> AllStories
        {
            get
            {
                return new RestRepository().GetAllStories().ToList();
            }
        }
    }
}
