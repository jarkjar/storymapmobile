﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Entities;
using Domain.Services;
using StoryMap.RestApi.v1.Entities;

namespace StoryMap.RestApi.v1.Repository
{
    public class RestRepository
    {
        int cacheDurationRestKeyword = 60;
        int cacheDurationRestStories = 120;
        int cacheDurationRestPlaces = 60;

        readonly string cacheKeyKeywords = "Cache_AllRestKeywordsPublished_V1";
        readonly string cacheKeyStoriesPublished = "Cache_AllRestStoriesPublished_V1";
        readonly string cacheKeyPlaces = "Cache_AllRestPlaces_V1";

        StoryRepository repository = new StoryRepository();

        public IEnumerable<RestKeyword> GetAllKeywords()
        {
            var cache = new StoryMapCache<IEnumerable<RestKeyword>>(cacheKeyKeywords);
            var restKeywords = cache.Get();
            if (restKeywords != null)
                return restKeywords;

            restKeywords = new List<RestKeyword>();
            restKeywords = from Keyword keyword in repository.GetAllKeywords()
                           select TransformKeyword(keyword);
            cache.Set(restKeywords, cacheDurationRestKeyword);
            return restKeywords;
        }

        public IEnumerable<RestStory> GetAllStories()
        {
            var cache = new StoryMapCache<List<RestStory>>(cacheKeyStoriesPublished);
            var restStories = cache.Get();
            if (restStories != null)
                return restStories;

            restStories = new List<RestStory>();
            foreach (var story in repository.GetAllStories())
            {
                var restStory = TransformStory(story);
                foreach (var keyword in story.Keywords)
                {
                    restStory.Keywords.Add(TransformKeyword(keyword));
                }
                restStories.Add(restStory);
            }
            cache.Set(restStories, cacheDurationRestStories);
            return restStories;
        }

        public IEnumerable<RestPlace> GetAllPlaces()
        {
            var cache = new StoryMapCache<List<RestPlace>>(cacheKeyPlaces);
            var restPlaces = cache.Get();
            if (restPlaces != null)
                return restPlaces;

            restPlaces = new List<RestPlace>();
            foreach (var place in repository.GetAllPlaces())
            {
                var restPlace = TransformPlace(place);
                foreach (var keyword in place.Keywords)
                {
                    restPlace.Keywords.Add(TransformKeyword(keyword));
                }
                foreach (var story in place.Stories)
                {
                    restPlace.Stories.Add(TransformStory(story));
                }
                restPlaces.Add(restPlace);
            }
            cache.Set(restPlaces, cacheDurationRestPlaces);
            return restPlaces;
        }

        private RestKeyword TransformKeyword(Keyword keyword)
        {
            if (keyword == null)
                return null;
            var restKeyword = new RestKeyword();
            restKeyword.Id = keyword.Id;
            restKeyword.Name = keyword.Name;
            return restKeyword;
        }

        private RestStory TransformStory(Story story)
        {
            if (story == null)
                return null;
            var restStory = new RestStory();
            restStory.Id = story.Id;
            restStory.Title = story.Title;
            restStory.Body = story.Body;
            restStory.CreatedDate = story.CreatedDate;
            restStory.EditedDate = story.EditedDate;
            restStory.Place = TransformPlace(story.Place);
            restStory.Editor = TransformUser(story.Editor);
            restStory.Creator = TransformUser(story.Creator);
            return restStory;
        }

        private RestUser TransformUser(User user)
        {
            if (user == null)
                return null;
            var restUser = new RestUser();
            restUser.Id = user.Id;
            restUser.CreatedDate = user.CreatedDate;
            restUser.Description = user.Description;
            restUser.EditedDate = user.EditedDate;
            restUser.Username = user.Username;
            return restUser;
        }

        private RestPlace TransformPlace(Place place)
        {
            if (place == null)
                return null;
            var restPlace = new RestPlace();
            restPlace.Id = place.Id;
            restPlace.Latitude = place.Location.Latitude.Value;
            restPlace.Longitude = place.Location.Longitude.Value;
            restPlace.Name = place.Name;
            return restPlace;
        }

    }
}