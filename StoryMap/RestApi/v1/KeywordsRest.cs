﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Routing;
using System.Xml.Serialization;
using StoryMap.RestApi.v1.Entities;
using StoryMap.RestApi.v1.Repository;

namespace StoryMap.RestApi.v1
{
    public class KeywordsRest : IHttpHandler, IRestHandler
    {
        public RouteValueDictionary RouteData { get; set; }

        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            var keywordsRoot = new KeywordsRoot();
            keywordsRoot.Keywords = AllKeywords;

            OutputHelper.Output<KeywordsRoot>(context, keywordsRoot);
        }

        [XmlRoot(ElementName = "Keywords")]
        public class KeywordsRoot
        {
            [XmlElement("Keyword")]
            public IEnumerable<RestKeyword> Keywords { get; set; }
        }

        public static IEnumerable<RestKeyword> AllKeywords
        {
            get
            {
                return new RestRepository().GetAllKeywords();
            }
        }
    }
}
