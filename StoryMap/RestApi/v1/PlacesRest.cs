﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Xml.Serialization;
using StoryMap.RestApi.v1.Entities;
using StoryMap.RestApi.v1.Repository;

namespace StoryMap.RestApi.v1
{
    public class PlacesRest : IHttpHandler, IRestHandler
    {
        public RouteValueDictionary RouteData { get; set; }

        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            var keywordsRoot = new PlacesRoot();
            keywordsRoot.Places = AllPlaces;

            OutputHelper.Output<PlacesRoot>(context, keywordsRoot);
        }

        [XmlRoot(ElementName = "Places")]
        public class PlacesRoot
        {
            [XmlElement("Place")]
            public List<RestPlace> Places { get; set; }
        }

        public static List<RestPlace> AllPlaces
        {
            get
            {
                return new RestRepository().GetAllPlaces().ToList();
            }
        }
    }
}