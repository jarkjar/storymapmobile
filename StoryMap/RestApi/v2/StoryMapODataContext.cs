﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using StoryMap.RestApi.v2.Entities;
using StoryMap.RestApi.v2.Repositories;

namespace StoryMap.RestApi.v2
{
    public class StoryMapODataContext
    {
        RestRepository repository = new RestRepository();

        public IQueryable<RestKeyword> Keywords
        {
            get
            {
                return repository.GetAllKeywords();
            }
        }

        public IQueryable<RestPlace> Places
        {
            get
            {
                return repository.GetAllPlaces();
            }
        }

        public IQueryable<RestStory> Stories
        {
            get
            {
                return repository.GetAllStories();
            }
        }
    }
}