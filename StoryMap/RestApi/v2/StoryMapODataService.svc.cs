using System;
using System.Collections.Generic;
using System.Data.Services;
using System.Data.Services.Common;
using System.Linq;
using System.ServiceModel.Web;
using System.Web;
using Domain.Services;
using StoryMap.RestApi.v2.Entities;

namespace StoryMap.RestApi.v2
{
    public class StoryMapODataService : DataService<StoryMapODataContext>
    {
        public static void InitializeService(DataServiceConfiguration config)
        {
            // Examples:
            // config.SetEntitySetAccessRule("MyEntityset", EntitySetRights.AllRead);
            // config.SetServiceOperationAccessRule("MyServiceOperation", ServiceOperationRights.All);
            config.SetEntitySetAccessRule("Keywords", EntitySetRights.AllRead);
            config.SetEntitySetAccessRule("Places", EntitySetRights.AllRead);
            config.SetEntitySetAccessRule("Stories", EntitySetRights.AllRead);
            //config.SetServiceOperationAccessRule("GetNearestPlaces", ServiceOperationRights.AllRead);
#if DEBUG
            config.UseVerboseErrors = true;
#endif
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V3;
        }

        //[WebGet]
        //public IQueryable<RestPlace> GetNearestPlaces(double nwLat, double nwLng, double seLat, double seLng)
        //{
        //    return from place in this.CurrentDataSource.Places
        //           where place.Latitude on tarpeeksi l�hell� nwLat jne...
        //}
    }
}
