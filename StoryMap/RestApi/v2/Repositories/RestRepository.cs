﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Domain.Entities;
using Domain.Services;
using StoryMap.RestApi.v2.Entities;
using System.Data.Entity;

namespace StoryMap.RestApi.v2.Repositories
{
    public class RestRepository
    {
        int cacheDurationRestKeyword = 30;
        int cacheDurationRestStories = 30;
        int cacheDurationRestPlaces = 30;

        readonly string cacheKeyKeywords = "Cache_AllRestKeywordsPublished_V2";
        readonly string cacheKeyStoriesPublished = "Cache_AllRestStoriesPublished_V2";
        readonly string cacheKeyPlaces = "Cache_AllRestPlaces_V2";

        StoryRepository repository = new StoryRepository();

        public IQueryable<RestKeyword> GetAllKeywords()
        {
            var cache = new StoryMapCache<IQueryable<RestKeyword>>(cacheKeyKeywords);
            var restKeywords = cache.Get();
            if (restKeywords != null)
                return restKeywords;

            restKeywords = from Keyword keyword in repository.GetAllKeywords()
                           select TransformKeyword(keyword);
            cache.Set(restKeywords, cacheDurationRestKeyword);
            return restKeywords.AsQueryable();
        }

        public IQueryable<RestPlace> GetAllPlaces()
        {
            var cache = new StoryMapCache<IQueryable<RestPlace>>(cacheKeyPlaces);
            var restPlaces = cache.Get();
            if (restPlaces != null)
                return restPlaces;

            restPlaces = from Place place in repository.GetAllPlaces()
                         select TransformPlace(place, true);
            cache.Set(restPlaces, cacheDurationRestPlaces);
            return restPlaces;
        }

        public IQueryable<RestStory> GetAllStories()
        {
            var cache = new StoryMapCache<IQueryable<RestStory>>(cacheKeyStoriesPublished);
            var restStories = cache.Get();
            if (restStories != null)
                return restStories;

            restStories = from Story story in repository.GetAllStories()
                          select TransformStory(story, true);
            cache.Set(restStories, cacheDurationRestStories);
            return restStories;
        }

        private RestKeyword TransformKeyword(Keyword keyword)
        {
            if (keyword == null)
                return null;
            var restKeyword = new RestKeyword();
            restKeyword.Id = keyword.Id;
            restKeyword.Name = keyword.Name;
            return restKeyword;
        }

        private RestPlace TransformPlace(Place place, bool includeChilds)
        {
            if (place == null)
                return null;
            var restPlace = new RestPlace();
            restPlace.Id = place.Id;
            restPlace.Latitude = place.Location.Latitude.Value;
            restPlace.Longitude = place.Location.Longitude.Value;
            restPlace.Name = place.Name;
            if (includeChilds)
                restPlace.Keywords = (from Keyword keyword in place.Keywords
                                      select TransformKeyword(keyword))
                                     .AsQueryable();
            if (includeChilds)
                restPlace.Stories = (from Story story in place.Stories
                                     select TransformStory(story, !includeChilds))
                                    .AsQueryable();

            return restPlace;
        }

        private RestStory TransformStory(Story story, bool includeChilds)
        {
            if (story == null)
                return null;
            var restStory = new RestStory();
            restStory.Id = story.Id;
            restStory.Title = story.Title;
            restStory.Body = story.Body;
            restStory.CreatedDate = story.CreatedDate;
            restStory.EditedDate = story.EditedDate;
            if (includeChilds)
                restStory.Place = TransformPlace(story.Place, !includeChilds);
            restStory.Editor = TransformUser(story.Editor);
            restStory.Creator = TransformUser(story.Creator);
            return restStory;
        }

        private RestUser TransformUser(User user)
        {
            if (user == null)
                return null;
            var restUser = new RestUser();
            restUser.Id = user.Id;
            restUser.CreatedDate = user.CreatedDate;
            restUser.Description = user.Description;
            restUser.EditedDate = user.EditedDate;
            restUser.Username = user.Username;
            return restUser;
        }
    }
}