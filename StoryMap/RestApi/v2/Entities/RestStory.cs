﻿using System;
using System.Collections.Generic;
using System.Data.Services.Common;

namespace StoryMap.RestApi.v2.Entities
{
    [DataServiceKey("Id")]
    public class RestStory
    {
        public RestStory()
        {
            Keywords = new List<RestKeyword>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public RestPlace Place { get; set; }
        public ICollection<RestKeyword> Keywords { get; set; }
        //public ICollection<RestMultimedia> Multimedia { get; set; }
        //public ICollection<RestUser> InvolvedUsers { get; set; }

        //public bool Published { get; set; }
        public RestUser Creator { get; set; }
        public RestUser Editor { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? EditedDate { get; set; }
    }
}