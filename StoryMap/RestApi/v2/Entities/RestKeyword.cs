﻿using System.Data.Services.Common;

namespace StoryMap.RestApi.v2.Entities
{
    [DataServiceKey("Name")]
    public class RestKeyword
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}