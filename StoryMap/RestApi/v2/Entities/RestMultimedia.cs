﻿using System;
using System.Collections.Generic;
using System.Data.Services.Common;

namespace StoryMap.RestApi.v2.Entities
{
    [DataServiceKey("Id")]
    public class RestMultimedia
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Extension { get; set; }
        /// <summary>
        /// Size in MB
        /// </summary>
        public int Size { get; set; }
        //public ICollection<RestKeyword> Keywords { get; set; }
        public RestStory Story { get; set; }

        //public bool Published { get; set; }
        //public RestUser Creator { get; set; }
        //public RestUser Editor { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? EditedDate { get; set; }
    }
}