﻿using System;
using System.Data.Services;
using System.ServiceModel.Activation;
using System.Web;
using System.Web.Routing;
using StoryMap.RestApi.v1;
using StoryMap.RestApi.v2;

namespace StoryMap
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            RouteTable.Routes.Add(new ServiceRoute("api/v2", new DataServiceHostFactory(), typeof(StoryMapODataService)));

            //RouteTable.Routes.Add("RestKeywords", new Route("api/v1/keywords",
            //    new RestRouteHandler<KeywordsRest>()));
            //RouteTable.Routes.Add("RestPlaces", new Route("api/v1/places",
            //    new RestRouteHandler<PlacesRest>()));
            //RouteTable.Routes.Add("RestStories", new Route("api/v1/stories",
            //    new RestRouteHandler<StoriesRest>()));

            RouteTable.Routes.MapPageRoute("ShowStoriesRoute", "ShowStories/{story}", "~/ShowStories.aspx");
            RouteTable.Routes.MapPageRoute("CreateStoryRoute", "Create", "~/CreateStory.aspx");
            RouteTable.Routes.MapPageRoute("RegisterRoute", "Register", "~/Register.aspx");
            RouteTable.Routes.MapPageRoute("LoginRoute", "Login", "~/Login.aspx");
            RouteTable.Routes.MapPageRoute("ManageRoute", "Manage", "~/Manage.aspx");
            RouteTable.Routes.MapPageRoute("DefaultRoute", "Default", "~/Default.aspx");
        }

        private class RestRouteHandler<THttpHandler> : IRouteHandler
            where THttpHandler : IHttpHandler, IRestHandler, new()
        {
            public IHttpHandler GetHttpHandler(RequestContext requestContext)
            {
                var handler = new THttpHandler();
                handler.RouteData = requestContext.RouteData.Values;
                return handler;
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}