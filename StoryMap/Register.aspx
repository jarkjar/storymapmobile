﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="StoryMap.Register" %>

<!DOCTYPE html> 
<html> 
	<head id="Head1" runat="server"> 
	<title>Sign up</title>
    <script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
    <script src="http://code.jquery.com/ui/1.9.1/jquery-ui.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
</head> 
<body> 

<!-- start page -->   

<div data-role="dialog">
    
    <!-- start header -->
    <div data-role="header" data-position="inline" data-nobackbtn="true">
        <h1>Sign up</h1>
    </div>
    <!-- end header -->

    <!-- start content -->
    <div data-role="content" data-theme="c" data-inset="true">

        <form runat="server">
            <asp:CreateUserWizard runat="server" ID="RegisterUser" ViewStateMode="Disabled" OnCreatedUser="RegisterUser_CreatedUser">
                <LayoutTemplate>
                    <asp:PlaceHolder runat="server" ID="wizardStepPlaceholder" />
                    <asp:PlaceHolder runat="server" ID="navigationPlaceholder" />
                </LayoutTemplate>
                <WizardSteps>
                    <asp:CreateUserWizardStep runat="server" ID="RegisterUserWizardStep">
                        <ContentTemplate>
                            <%--<p class="message-info">
                        Passwords are required to be a minimum of <%: Membership.MinRequiredPasswordLength %> characters in length.
                    </p>--%>

                            <fieldset>
                                <%--<legend>Registration Form</legend>--%>

                                <asp:Literal runat="server" ID="ErrorMessage" />

                                <asp:Label ID="Label1" runat="server" AssociatedControlID="UserName">Username:</asp:Label>
                                <asp:TextBox runat="server" ID="UserName" />
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="UserName" ErrorMessage="The user name field is required." />--%>

                                <asp:Label ID="Label2" runat="server" AssociatedControlID="Email">Email:</asp:Label>
                                <asp:TextBox runat="server" ID="Email" TextMode="Email" />
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Email" ErrorMessage="The email address field is required." />--%>

                                <asp:Label ID="Label3" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                                <asp:TextBox runat="server" ID="Password" TextMode="Password" />
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="Password" ErrorMessage="The password field is required." />--%>

                                <asp:Label ID="Label4" runat="server" AssociatedControlID="ConfirmPassword">Confirm password:</asp:Label>
                                <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" />
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ConfirmPassword" Display="Dynamic" ErrorMessage="The confirm password field is required." />--%>
                                <%--<asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword" Display="Dynamic" ErrorMessage="The password and confirmation password do not match." />--%>

                                <asp:Button ID="Button1" runat="server" CommandName="MoveNext" Text="Register" data-role="button" data-inline="true" data-theme="b" />
                            </fieldset>
                        </ContentTemplate>
                        <CustomNavigationTemplate />
                    </asp:CreateUserWizardStep>
                </WizardSteps>
            </asp:CreateUserWizard>
        </form>

    </div>
    <!-- end content -->

</div>

<!-- end page -->

</body>
</html>
