﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Domain.Services;

namespace StoryMap
{
    public partial class CreateStory : System.Web.UI.Page
    {
        StoryRepository repository = new StoryRepository();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string slat = Request.QueryString["lat"];
                hdnLatitude.Value = slat;

                string slng = Request.QueryString["lng"];
                hdnLongitude.Value = slng;

                string placename = Request.QueryString["name"];
                tbPlace.Text = placename;
            }
        }

        //NoBot ajaxcontrollin laskukaava lähetettäväksi selaimelle
        protected void PageNoBot_GenerateChallengeAndResponse(object sender, AjaxControlToolkit.NoBotEventArgs e)
        {
            Random r = new Random();
            int iFirst = r.Next(100);
            int iSecond = r.Next(100);
            e.ChallengeScript = String.Format("eval('{0}+{1}')", iFirst, iSecond);
            e.RequiredResponse = Convert.ToString(iFirst + iSecond);
        }

        protected void btnCreateStory_Click(object sender, EventArgs e)
        {
            string title = tbTitle.Text;
            string body = tbStory.Text;
            string placeName = tbPlace.Text;
            string tags = tbTags.Text;

            // Check required fields
            // TODO: Add validations
            if (string.IsNullOrEmpty(title) || string.IsNullOrEmpty(body) ||
                string.IsNullOrEmpty(placeName) || string.IsNullOrEmpty(tags))
                return;

            double longitude;
            if (!double.TryParse(hdnLongitude.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out longitude))
                longitude = 0;

            double latitude;
            if (!double.TryParse(hdnLatitude.Value, NumberStyles.Float, CultureInfo.InvariantCulture, out latitude))
                latitude = 0;

            if (latitude == 0 || longitude == 0)
                Response.Redirect("./?msg=1&cd=1");

            bool success = false;                        
            bool publish = true;
            success = repository.CreateStory(placeName, latitude, longitude, title, body, tags, publish);
            
            if (success)
                Response.Redirect("./?msg=0&cd=1");
            else
                Response.Redirect("./?msg=1&cd=1");
        }
    }
}