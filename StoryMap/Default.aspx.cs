﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Domain.Entities;
using Domain.Services;

namespace StoryMap
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            pnlInfoMessage.Visible = false;
            string msg = Request.QueryString["msg"];
            if (!string.IsNullOrEmpty(msg))
                ParseMessage(msg);
        }

        private void ParseMessage(string msg)
        {
            int msgId;
            if (int.TryParse(msg, out msgId))
            {
                string message = string.Empty;
                string cssClass = string.Empty;
                switch (msgId)
                {
                    case 0:
                        message = "Story saved! It will be shown on the map soon...";
                        cssClass = "info";
                        break;
                    case 1:
                        message = "Error when saving story. Try again later.";
                        cssClass = "error";
                        break;
                    default:
                        pnlInfoMessage.Visible = false;
                        break;
                }
                if (!string.IsNullOrEmpty(cssClass) && !string.IsNullOrEmpty(message))
                {
                    infoMessage.Attributes.Add("class", cssClass);
                    lblInfo.Text = message;
                    pnlInfoMessage.Visible = true;
                }
            }
        }
    }
}