﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="StoryMap.Login" %>

<!DOCTYPE html> 
<html> 
<head>
    <title>Login</title>
    <script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
    <script src="http://code.jquery.com/ui/1.9.1/jquery-ui.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
</head> 
<body> 

<!-- start page -->
<div data-role="dialog">

	<!-- start header -->
	<div data-role="header">
		<h1>Login</h1>
	</div>
	<!-- end header -->

	<!-- start content -->
	<div data-role="content" data-inset="true">	
	
        <form runat="server">
            <asp:Login ID="Login1" runat="server" ViewStateMode="Disabled" RenderOuterTable="false">
                <LayoutTemplate>
                    <fieldset>
                        <%--<legend>Log in Form</legend>--%>

                        <asp:Literal runat="server" ID="FailureText" />

                        <asp:Label ID="Label1" runat="server" AssociatedControlID="UserName">Username:</asp:Label>
                        <asp:TextBox runat="server" ID="UserName" />
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="UserName" ErrorMessage="The email field is required." />--%>

                        <asp:Label ID="Label2" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                        <asp:TextBox runat="server" ID="Password" TextMode="Password" />
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Password" ErrorMessage="The password field is required." />--%>

                        <%--<asp:CheckBox runat="server" ID="RememberMe" />
                        <asp:Label ID="Label3" runat="server" AssociatedControlID="RememberMe">Remember me?</asp:Label>--%>

                        <asp:Button ID="Button1" runat="server" CommandName="Login" Text="Login" data-role="button" data-inline="true" data-theme="b" />
                        
                        <hr />
                        Don't have a login? <a href="Register.aspx" data-rel="dialog">Sign up</a>
                    </fieldset>
                </LayoutTemplate>
            </asp:Login>
        </form>

	</div>
	<!-- end content -->
	
</div>

<!-- end page -->

</body>
</html>
