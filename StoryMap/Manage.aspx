﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Manage.aspx.cs" Inherits="StoryMap.Manage" %>

<!DOCTYPE html> 
<html> 
<head>
    <title><%: User.Identity.Name %></title>
    <script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
    <script src="http://code.jquery.com/ui/1.9.1/jquery-ui.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
</head> 
<body> 

<!-- start page -->
<div data-role="page">

	<!-- start header -->
	<div data-role="header">
		<h1>Account - <%: User.Identity.Name %></h1>
	</div>
	<!-- end header -->

	<!-- start content -->
	<div data-role="content" data-inset="true">

        <%--<asp:PlaceHolder runat="server" ID="successMessage" Visible="false" ViewStateMode="Disabled">
            <p class="message-success"><%: SuccessMessage %></p>
        </asp:PlaceHolder>--%>

        <%--<p>You're logged in as <strong><%: User.Identity.Name %></strong>.</p>--%>

        <form runat="server">
            <asp:ChangePassword ID="ChangePassword1" runat="server" CancelDestinationPageUrl="~/" ViewStateMode="Disabled" RenderOuterTable="false" SuccessPageUrl="Manage.aspx?m=ChangePwdSuccess">
                <ChangePasswordTemplate>
                    <fieldset>
                        <%--<legend>Change password details</legend>--%>

                        <asp:Literal runat="server" ID="FailureText" />

                        <asp:Label runat="server" ID="CurrentPasswordLabel" AssociatedControlID="CurrentPassword">Current password</asp:Label>
                        <asp:TextBox runat="server" ID="CurrentPassword" TextMode="Password" />
                        <%--                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="CurrentPassword" ErrorMessage="The current password field is required." ValidationGroup="ChangePassword" />--%>

                        <asp:Label runat="server" ID="NewPasswordLabel" AssociatedControlID="NewPassword">New password</asp:Label>
                        <asp:TextBox runat="server" ID="NewPassword" TextMode="Password" />
                        <%--                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="NewPassword" ErrorMessage="The new password is required." ValidationGroup="ChangePassword" />--%>

                        <asp:Label runat="server" ID="ConfirmNewPasswordLabel" AssociatedControlID="ConfirmNewPassword">Confirm new password</asp:Label>
                        <asp:TextBox runat="server" ID="ConfirmNewPassword" TextMode="Password" />
                        <%--                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ConfirmNewPassword" Display="Dynamic" ErrorMessage="Confirm new password is required." ValidationGroup="ChangePassword" />--%>
                        <%--                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="NewPassword" ControlToValidate="ConfirmNewPassword" Display="Dynamic" ErrorMessage="The new password and confirmation password do not match." ValidationGroup="ChangePassword" />--%>

                        <asp:Button ID="Button1" runat="server" CommandName="ChangePassword" Text="Change password" ValidationGroup="ChangePassword" />
                    </fieldset>
                </ChangePasswordTemplate>
            </asp:ChangePassword>
        </form>

	</div>
	<!-- end content -->
	
</div>
<!-- end page -->

</body>
</html>
