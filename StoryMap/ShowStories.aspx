﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ShowStories.aspx.cs" Inherits="StoryMap.ShowStories" %>

<!DOCTYPE html>
<html>
<head>
    <title style="font-family: 'Agency FB'">Confestigate</title>
    <meta name="viewport" content="width=device-width; height=device-height; initial-scale=1.0; user-scalable=yes">
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.css" />
    <%--<link rel="StyleSheet" href="css/ui-lightness/jquery-ui-1.8.6.custom.css" type="text/css" media="all" />--%>
    <link rel="stylesheet" href="css/main.css" />
    <script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
    <script src="http://code.jquery.com/ui/1.9.1/jquery-ui.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.2.0/jquery.mobile-1.2.0.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <ajaxToolkit:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" EnableScriptGlobalization="true"
            EnableScriptLocalization="true" />
        <script type="text/javascript">
            function bindEvents() {
                $(document).trigger('create');
            }

            bindEvents();

            //Re-bind for callbacks     
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_endRequest(function () {
                bindEvents();
            });
        </script>
        <!-- Stories page -->
        <div data-role="page" id="stories">
            <script>
                $.mobile.ajaxEnabled = false;
            </script>
            <div data-role="header">
                <div data-role="navbar">
                    <ul>
                        <li><a id="btnNearStories" class="ui-btn-active ui-state-persist" href="ShowStories\near" data-role="button">Story</a></li>
                        <li>
                            <asp:Button ID="btnMap" runat="server" data-ajax="false" data-role="button" Text="Map" OnClick="btnMap_Click" /></li>
                    </ul>
                </div>
            </div>
            <div data-role="content" data-theme="a" data-inset="true">
                <asp:UpdatePanel ID="upTagList" runat="server">
                    <ContentTemplate>
                        <div id="tagList">
                            <asp:ListView ID="lvTags" runat="server" OnItemCommand="lvTags_ItemCommand">
                                <LayoutTemplate>
                                    <span style="font-weight: bold">Keywords:</span>
                                    <span data-role="controlgroup" data-type="horizontal">
                                        <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                                    </span>
                                    <asp:Panel ID="pnlRemoveFilter" runat="server">
                                        <span>
                                            <asp:Button ID="btnRemoveFilter" data-role="button" data-inline="true" runat="server"
                                                Text="Remove filter" data-icon="delete" CommandName="Remove" />
                                        </span>
                                    </asp:Panel>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <asp:Button data-role="button" data-inline="true" CommandArgument='<%# Eval("Id") %>'
                                        CommandName="Filter" runat="server" Text='<%# Eval("Name") %>' />
                                </ItemTemplate>
                                <SelectedItemTemplate>
                                    <asp:Button class="ui-btn-active ui-state-persist" ID="Button1" data-role="button" data-inline="true" CommandArgument='<%# Eval("Id") %>'
                                        CommandName="Filter" runat="server" Text='<%# Eval("Name") %>' />
                                </SelectedItemTemplate>
                            </asp:ListView>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div id="storyList">
                    <asp:UpdatePanel ID="upStoryList" runat="server" UpdateMode="Conditional">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="lvTags" />
                        </Triggers>
                        <ContentTemplate>
                            <asp:ListView ID="lvStories" runat="server">
                                <LayoutTemplate>
                                    <h3>Stories:</h3>
                                    <div data-role="collapsible-set">
                                        <asp:PlaceHolder ID="itemPlaceholder" runat="server" />
                                    </div>
                                </LayoutTemplate>
                                <EmptyDataTemplate>
                                    <p style="text-align: center">Doh... No stories found. You can try to increase radius value from settings. It is also possible there are no stories near by selected location.</p>
                                </EmptyDataTemplate>
                                <ItemTemplate>
                                    <div data-role="collapsible">
                                        <h3><%# Eval("Title") %></h3>
                                        <%--<span style="float: right">
                                            <asp:HyperLink ID="hlShowOnMap" runat="server" data-ajax="false"
                                                data-role="button" Text="Show on map" data-inline="true" NavigateUrl='<%# Eval("MapUrl") %>' />
                                        </span>--%>
                                        <p><%# Eval("Body") %></p>
                                        <span>
                                            <strong>Created: </strong><%# Eval("CreatedDate", "{0:g}") %>&nbsp;<%# Eval("Creator") %>
                                        </span>
                                        <span>
                                            <strong>Keywords: </strong><%# Eval("KeywordList") %>
                                        </span>
                                    </div>
                                </ItemTemplate>
                                <SelectedItemTemplate>
                                    <div data-role="collapsible" data-collapsed="false">
                                        <h3><%# Eval("Title") %></h3>
                                        <%--<span style="float: right">
                                    <asp:HyperLink ID="hlShowOnMap" runat="server" data-ajax="false"
                                        data-role="button" Text="Show on map" data-inline="true" NavigateUrl='<%# Eval("MapUrl") %>' />
                                </span>--%>
                                        <p><%# Eval("Body") %></p>
                                        <div>
                                            <strong>Created: </strong><%# Eval("CreatedDate", "{0:g}") %>&nbsp;<%# Eval("Creator") %>
                                        </div>
                                        <div>
                                            <strong>Keywords: </strong><%# Eval("KeywordList") %>
                                        </div>
                                    </div>
                                </SelectedItemTemplate>
                            </asp:ListView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div data-role="footer" data-position="fixed">
            </div>
        </div>
        <!-- end of stories page -->
    </form>
</body>
</html>
