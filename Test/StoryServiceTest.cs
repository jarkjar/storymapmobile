﻿using System;
using System.Collections.Generic;
using Domain.Entities;
using Domain.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Test
{
    [TestClass]
    public class StoryServiceTest
    {
        [TestMethod]
        public void CreateStoryTest_WhenCreated_ThenItShouldFoundFromContext()
        {
            // Generate dummy keywords, add diplicates and few whitespaces
            var keys = new List<string>() { Guid.NewGuid().ToString(), Guid.NewGuid().ToString(), Guid.NewGuid().ToString() };
            var tags = string.Format(",   , {0}, {1} ,, , {2}, {2} , {1}  ,   {0},  ,", keys.ToArray());
            var target = new StoryRepository();
            var title = Guid.NewGuid().ToString();
            var body = Guid.NewGuid().ToString();
            bool published = true;
            var placeName = Guid.NewGuid().ToString();
            var location = target.ConvertToDbGeography(new Random().NextDouble() * 70, new Random().NextDouble() * 22);

            bool success = target.CreateStory(placeName, location.Latitude.Value, location.Longitude.Value, title, body, tags, published);
            Assert.IsTrue(success);

            var actual = target.GetStoryByTitle(title);
            Assert.IsNotNull(actual);
            Assert.AreEqual(title, actual.Title);
            Assert.AreEqual(body, actual.Body);
            CollectionAssert.AreEquivalent(keys, actual.Keywords.Select(k => k.Name).ToList());
            Assert.AreEqual(placeName, actual.Place.Name);
            Assert.AreEqual(location.Latitude.Value, actual.Place.Location.Latitude.Value);
            Assert.AreEqual(location.Longitude.Value, actual.Place.Location.Longitude.Value);
            Assert.AreEqual(published, actual.Published);
        }

        [TestMethod]
        public void GetKeywordsTest()
        {
            // Generate dummy keywords, add diplicates and few whitespaces
            var keys = new List<string>() { Guid.NewGuid().ToString(), Guid.NewGuid().ToString(), Guid.NewGuid().ToString() };
            var tags = string.Format(",   , {0}, {1} ,, , {2}, {2} , {1}  ,   {0},  ,", keys.ToArray());
            var target = new StoryRepository();
            var title = Guid.NewGuid().ToString();
            var body = Guid.NewGuid().ToString();
            var location = target.ConvertToDbGeography(new Random().NextDouble() * 70, new Random().NextDouble() * 22);
            var placeName = Guid.NewGuid().ToString();
            bool published = true;

            bool success = target.CreateStory(placeName, location.Latitude.Value, location.Longitude.Value, title, body, tags, published);
            Assert.IsTrue(success);

            var actual = target.GetStoryByTitle(title);
            Assert.IsNotNull(actual);
            Assert.AreEqual(title, actual.Title);
            Assert.AreEqual(body, actual.Body);
            CollectionAssert.AreEquivalent(keys, actual.Keywords.Select(k => k.Name).ToList());
            Assert.AreEqual(placeName, actual.Place.Name);
            Assert.AreEqual(location.Latitude.Value, actual.Place.Location.Latitude.Value);
            Assert.AreEqual(location.Longitude.Value, actual.Place.Location.Longitude.Value);
            Assert.AreEqual(published, actual.Published);

            // Check keyword list contains currently created keys
            var allKeys = target.GetAllKeywords();
            Assert.IsTrue(allKeys.Any(i => keys.Contains(i.Name)));
        }
    }
}
